function GalleriesContainer(container) {
    var galleriesContainer = container;

    function parseGalleriesData(dataArray) {
        dataArray.forEach(function(item) {
            console.log(item.name);
            var template= `
                <h3 class="">${item.name}</h3>
                <div class="">${item.description}</div>
                <div id="gallery-container-${item.id}"></div>
            `;
            var galleryId=`#gallery-container-${item.id}`;
            galleriesContainer.innerHTML+=template;

            var galleryContainer = new GalleryContainer(document.querySelector(galleryId));
            galleryContainer.invalidate(item.applications);
        });
    }

    function invalidate(dataArray) {
        clearGallery();
        parseGalleriesData(dataArray)
    }
    function clearGallery() {
        galleriesContainer.innerHTML = "";
    }
    return {
        invalidate: invalidate
    }
};

var galleriesContainer = new GalleriesContainer(document.querySelector("#galleries-container"));
galleriesContainer.invalidate(galleriesData);