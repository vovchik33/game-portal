// Это реальный объект на основе которого вам надо будет строить галлерею
var galleriesData = [
    {
        id: 1421153200631,
        name: "New&Updated Games",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }, 

    {
        id: 1421153200632,
        name: "Learn to play an instrument",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }, 

    {
        id: 1421153200633,
        name: "Multi-player Games",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }, 

    {
        id: 1421153200634,
        name: "Leaderboard Games",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }, 

    {
        id: 1421153200635,
        name: "Games We Are Playing",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }, 

    {
        id: 1421153200636,
        name: "Deal of the Week",
        description: "Here is an example of description",
        translate:{
            en: {name:"New&Updated Games", description:"Here is an example of description"},
        },
        applications: [
            {
                id: 1421153200636,
                name: "Game #0001",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200637,
                name: "Game #0002",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200638,
                name: "Game #0003",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            },
            {
                id: 1421153200639,
                name: "Game #0004",
                description: "Here is an example of description",
                preview: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
                url: "http://desktopwallpapers.org.ua",
                translate:{
                    en: {name:"New&Updated Games", description:"Here is an example of description"},
                },
                date: 1421153200637
            }
        ]
    }
];

/*
[{
	url: "http://desktopwallpapers.org.ua/mini/201507/40069.jpg",
	name: "CHEVROLET",
	id : 1,
	params: {
		status: true,
		progress: "80"
	},
	description : "Be conveyed to users of assistive technologies – such as",
	date : 1422153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40068.jpg",
	name: "DEWOO",
	id : 2,
	params: {
		status: true,
		progress: "88"
	},
	description : "sing color to add meaning to a button",
	date : 1421153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40067.jpg",
	name: "FOLKSWAGEN",
	id : 3,
	params: {
		status: true,
		progress: "64"
	},
	description : "be conveyed to users of assistive technologies",
	date : 1426153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40057.jpg",
	name: "FERRARI",
	id : 4,
	params: {
		status: true,
		progress: "38"
	},
	description : "ssistive technologies – such as screen readers. Ensure",
	date : 1428153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40066.jpg",
	name: "BMW",
	id : 5,
	params: {
		status: true,
		progress: "12"
	},
	description : "color to add meaning to a button only provides",
	date : 1402153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40064.jpg",
	name: "MERCEDESS",
	id : 6,
	params: {
		status: true,
		progress: "83"
	},
	description : "om the content itself (the visible text of the button)",
	date : 1442153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40063.jpg",
	name: "SKODA",
	id : 7,
	params: {
		status: true,
		progress: "49"
	},
	description : "r is either obvious from the content itself",
	date : 1482153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40062.jpg",
	name: "FORD",
	id : 8,
	params: {
		status: true,
		progress: "14"
	},
	description : "included through alternative means, such as additional text hidden with the",
	date : 1442153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40059.jpg",
	name: "TOYOTA",
	id : 9,
	params: {
		status: true,
		progress: "20"
	},
	description : "meaning to a button only provides a visual",
	date : 1322153200637
},{
	url: "http://desktopwallpapers.org.ua/mini/201507/40058.jpg",
	name: "RENAULT",
	id : 10,
	params: {
		status: true,
		progress: "40"
	},
	description : "uded through alternative means, such as additional",
	date : 1322159200637
}]
*/