function GalleryContainer(container) {
    var galleryContainer = container;

    function parseGalleriesData(dataArray) {
        dataArray.forEach(function(item) {
            console.log(item.name);
            var template=`\
                <div class="marked col-sm-4 col-xs-6 col-lg-3">\
                    <div class="thumbnail">
                        <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                        <div class="info-wrapper">\
                            <div><h4>${item.name}</h4></div>\
                            <div>${item.description}</div>\
                            <div>${item.date}</div>\
                        </div>\
                        <input class="btn btn-primary" id="${item.id}" type="button" value="Играть"/>
                    </div>\
                </div>\
            `;
            galleryContainer.innerHTML+=template;
        });
    }
    function invalidate(dataArray) {
        clearGallery();
        parseGalleriesData(dataArray)
    }
    function clearGallery() {
        galleryContainer.innerHTML = "";
    }
    return {
        invalidate: invalidate
    }
};